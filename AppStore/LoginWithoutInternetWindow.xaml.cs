﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppStore
{
    /// <summary>
    /// Interaction logic for LoginWithoutInternetWindow.xaml
    /// </summary>
    public partial class LoginWithoutInternetWindow : Window
    {
       
        public LoginWithoutInternetWindow()
        {
            InitializeComponent();
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {
           
            (new MainWindow()).Show();
            this.Close();
        }
        }
}
