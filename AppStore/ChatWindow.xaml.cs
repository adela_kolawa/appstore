﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Data;
using Newtonsoft.Json.Linq;
using Quobject.SocketIoClientDotNet.Client;
using System.Windows.Threading;
using System.ComponentModel;

namespace AppStore
{
    /// <summary>
    /// Interaction logic for ChatWindow.xaml
    /// </summary>
    public partial class ChatWindow : Window
    {
        Socket socket;
        
        public ChatWindow()
        {
            InitializeComponent();
            try
            {
                socket = IO.Socket(String.Format("http://localhost:3001", MainWindow.id));
                socket.On(Quobject.SocketIoClientDotNet.Client.Socket.EVENT_CONNECT, () => {
                    this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
                    {
                        this.textBlock.Text += String.Format("Polaczono z serwerem \n");
                    }));
                } );

                JObject json = new JObject(new JProperty("id", MainWindow.id));
                socket.Emit("registerSocket", json);

                socket.On("message", (data) =>
                 {
                     setMessage(data.ToString());
                 });
            }
            catch
            {
            }
        }
        private void setMessage(string msg)
        {
            this.Dispatcher.Invoke(DispatcherPriority.Normal, new Action(() =>
            {
                this.textBlock.Text += String.Format("User {0} : {1} \n", textBox2.Text, msg);
            }));
        }
        private void textBox_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (textBox2.Text != "")
            {
                if (e.Key == Key.Enter && textBox.Text!="")
                {
                    textBlock.Text += String.Format(" JA : {0} \n", textBox.Text);
                    

                    JObject json =  new JObject(new JProperty("targetUserId", textBox2.Text),
                                                new JProperty("message", textBox.Text) );

                    socket.Emit("message", json);
                    textBox.Clear();

                }
            }
        }

        private void Window_Closing_1(object sender, CancelEventArgs e)
        {
            //e.Cancel = true;
            JObject json = new JObject(new JProperty("id", MainWindow.id));

            socket.Emit("deleteSocket", json);
            socket.Disconnect();
        }
    }
}
