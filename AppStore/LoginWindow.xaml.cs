﻿using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppStore
{
    /// <summary>
    /// Interaction logic for LoginWindow.xaml
    /// </summary>
    ///

    public partial class LoginWindow : Window
    {
        public string Login { get; set; }
        public string Password { get; set; }
        public string Id { get; set; }

        LocalSave localSave = new LocalSave();
        public LoginWindow()
        {
            InitializeComponent();
        }
        private void button1_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        
        private void btnLogin_Click(object sender, RoutedEventArgs e)
        {

            Login = tBEmail.Text;
            Password = tBPassword.Password;
            if (Login == "" || Password == "")
            {
                MessageBoxResult result = MessageBox.Show("Podaj login i hasło!");

            }
            else
            {
                if (checkServerConnection())
                {
                    try
                    {
                        Newtonsoft.Json.Linq.JObject json = new JObject(
                            new JProperty("email", Login),
                            new JProperty("password", Password));
                        string result = HttpPost("http://localhost:3001/auth/login",json);
                        dynamic jsonResultPost = JsonConvert.DeserializeObject(result);
                        Id = (string)jsonResultPost.id;
                        MainWindow.login = Login;
                        MainWindow.id = Id;
                        dynamic jsonResultGet = HttpGet(String.Format(@"http://localhost:3001/games/fetchAllUserGames?id={0}",Id));
                        int size = jsonResultGet.games.Count;
                        for (int i=0; i<size; i++)
                        {
                            localSaveGame(jsonResultGet,i);
                        }
                        (new MainWindow()).Show();
                        this.Close();
                    }
                    catch
                    {
                        MessageBoxResult result = MessageBox.Show("Niepoprawny login lub hasło!");
                        MainWindow.login = Login;
                        (new LoginWindow()).Show();
                        this.Close();
                    }
                }
                else
                {
                    MainWindow.login = Login;
                    (new LoginWithoutInternetWindow()).Show();
                    this.Close();
                }
            }           
        }

        private bool checkServerConnection()
        {
            try
            {
                using (WebClient client = new WebClient())
                {
                    using (client.OpenRead("http://localhost:3000"))
                    {
                        return true;
                    }               
                }
            }
            catch
            {
                return false;
            }
        }

        public string HttpPost(string URI, JObject json)
        {
            HttpWebRequest request = (HttpWebRequest)
            WebRequest.Create(URI);
            request.KeepAlive = false;
            request.ProtocolVersion = HttpVersion.Version10;
            request.Method = "POST";

            string j = json.ToString();
            byte[] postBytes = Encoding.ASCII.GetBytes(j);
            // this is important - make sure you specify type this way
            request.ContentType = "application/json; charset=UTF-8";
            request.Accept = "application/json";
            request.ContentLength = postBytes.Length;
            Stream requestStream = request.GetRequestStream();

            // now send it
            requestStream.Write(postBytes, 0, postBytes.Length);
            requestStream.Close();

            // grab te response and print it out to the console along with the status code
            HttpWebResponse response = (HttpWebResponse)request.GetResponse();
            string result;
            using (StreamReader rdr = new StreamReader(response.GetResponseStream()))
            {
                result = rdr.ReadToEnd();
            }

            return result;
        }

        public dynamic HttpGet(string URI)
        {
            WebRequest wrGETURL = WebRequest.Create(URI);

            Stream objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            string result = objReader.ReadToEnd();

            dynamic json = JsonConvert.DeserializeObject(result);

            return json;
        }

        public void localSaveGame(dynamic json, int id)
        {
            localSave.saveGameName((string)json.games[id].name);
            localSave.saveGameVersion((string)json.games[id].name, (string)json.games[id].version);
            localSave.saveGameDescription((string)json.games[id].name, (string)json.games[id].description);

            localSave.downloadGameThumbnail((string)json.games[id].name, (string)json.games[id].thumbnail);
            localSave.downloadGameExe((string)json.games[id].name, (string)json.games[id].game);

        }

    }
}

