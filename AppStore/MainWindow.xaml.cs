﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace AppStore
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static string login;
        public static string id;
        dynamic json;
        LocalRead localRead = new LocalRead();
        public MainWindow()
        {
            InitializeComponent();
            try
            {
                getGames();
                string gamesToUpdate = checkNotification();
                if (gamesToUpdate != "")
                {
                    gamesToUpdate = gamesToUpdate.Insert(0, "Aktualizacje: \n");
                }
                tBNotification.Text = gamesToUpdate;
            }
            catch {
                button1.IsEnabled = false;
                button4.IsEnabled = false;
            }
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            (new Window2()).Show();
            this.Close();
        }

        private void button2_Click(object sender, RoutedEventArgs e)
        {
            (new Window3()).Show();
            this.Close();
        }

        private void button3_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
        private void button4_Click(object sender, RoutedEventArgs e)
        {
            (new ChatWindow()).Show();
        }
        public string checkNotification()
        {
            localRead.getFilesName();
            string gamesToUpdate = "";
            for (int i = 0; i < json.games.Count; i++)
            {
                if (!checkDownloadGame((string)json.games[i].name))
                {
                    if (json.games[i].version != localRead.getVersionForGameName((string)json.games[i].name))
                    {
                        gamesToUpdate = gamesToUpdate + "" + (string)json.games[i].name + "\n" +
                            "  nowa wersja: " + (string)json.games[i].version + "\n";
                    }
                }
            }

            return gamesToUpdate;
        }

        public void getGames()
        {
            string sURL = "http://localhost:3001/games/fetchallgames";
            WebRequest wrGETURL = WebRequest.Create(sURL);

            Stream objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            string result = objReader.ReadToEnd();

            json = JsonConvert.DeserializeObject(result);

        }
        public bool checkDownloadGame(string name)
        {
            localRead.getFilesName();
            foreach (var nameFile in localRead.namesFiles)
            {
                if (nameFile == name)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
