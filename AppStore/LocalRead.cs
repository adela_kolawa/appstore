﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Windows.Media.Imaging;
using System.Windows.Controls;

namespace AppStore
{
    class LocalRead
    {
        public DirectoryInfo myDir = new DirectoryInfo(String.Format(@"Games\Name\{0}",MainWindow.login));
        public List<string> namesFiles = new List<string>();
        public List<string> namesGames= new List<string>();
        public List<string> versionsGames = new List<string>();
        public List<string> descriptionGames = new List<string>();
        public List<Image> images = new List<Image>();

        public int countGame()
        {
            try
            {
                int count = myDir.GetFiles().Count();
                getFilesName();
                return count;
            }
            catch
            {
               return 0;
            }
        } 
        public void getFilesName()
        {
            List<FileInfo>  files = myDir.GetFiles().ToList();
            for(int i = 0; i < files.Count(); i++)
            {
                string name = files[i].Name;
                name = name.Substring(0, name.Length - 4); 
                namesFiles.Add(name);
             }
            readDataFromFIles();
            
         }

        public void readDataFromFIles()
        {
            for (int i = 0; i < namesFiles.Count(); i++)
            {
                getFromTxtFile(namesFiles[i]);
                getImagesFromFolder(namesFiles[i]);
            }
        }
        
        public string getVersionForGameName(string name)
        {
            var path = String.Format(@"Games\Version\{0}\{1}.txt", MainWindow.login,name);
            var file = new FileInfo(path);
            var version = file.OpenText();
            return version.ReadToEnd();
        }

        public void getFromTxtFile(string name)
        {
            string path = String.Format(@"Games\Name\{0}\{1}.txt", MainWindow.login, name);
            
            FileInfo file = new FileInfo(path);
            StreamReader s = file.OpenText();
            namesGames.Add(s.ReadToEnd());

            path = String.Format(@"Games\Version\{0}\{1}.txt", MainWindow.login, name);
            file = new FileInfo(path);
            s = file.OpenText();
            versionsGames.Add(s.ReadToEnd());

            path = String.Format(@"Games\Description\{0}\{1}.txt", MainWindow.login, name);
            file = new FileInfo(path);
            s = file.OpenText();
            descriptionGames.Add(s.ReadToEnd());
            s.Close();
        }

        public void getImagesFromFolder(string name)
        {
            string path = String.Format(@"Games/Image/{0}\{1}.jpg", MainWindow.login, name);
            Image image = new Image();
            image.Source = new BitmapImage(new Uri(path, UriKind.RelativeOrAbsolute));         
            images.Add(image);

        }

        public string getPathToExe(string name)
        {
            return String.Format(@"Games/Exe/{0}\{1}.exe", MainWindow.login,name);
        }
    }
}
