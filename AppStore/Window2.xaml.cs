﻿using System.Windows;
using System.Windows.Controls;
using System.Net;
using System.IO;
using System.Collections.Generic;
using System.Windows.Media.Imaging;
using Newtonsoft.Json;
using System;
using System.Windows.Media;

namespace AppStore
{
    /// <summary>
    /// Interaction logic for Window2.xaml
    /// </summary>
    public partial class Window2 : Window
    {
        LocalSave localSave = new LocalSave();
        LocalRead localRead = new LocalRead();
        dynamic json;

        public Window2()
        {
            getGames();
            InitializeComponent();
            generateView(json.games.Count);

        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {
            (new MainWindow()).Show();
            this.Close();
        }

        private void generateView(int number)
        {

            List<TextBlock> listTitle = new List<TextBlock>();
            List<TextBlock> listDescription = new List<TextBlock>();
            List<Image> listImg = new List<Image>();
            List<Button> listButton = new List<Button>();
            List<Button> listButtonNewVersion = new List<Button>();
            List<StackPanel> listStackPanel = new List<StackPanel>();
            int stackPanelX = 50;
            int stackPanelY = 30;
            int y = 0;
            int x = 0;
            for (int i=0; i<number; i++)
            {
                if (i>1 && i % 4 == 0)
                {
                    y += 350;
                    x = 0;
                }
                listStackPanel.Add(new StackPanel());
                listStackPanel[i].VerticalAlignment = VerticalAlignment.Top;
                listStackPanel[i].HorizontalAlignment = HorizontalAlignment.Left;
                listStackPanel[i].Width = 300;
                listStackPanel[i].Height = 500;
                listStackPanel[i].Margin = new Thickness(stackPanelX + x, stackPanelY + y, 0, 0);
                listStackPanel[i].Background = new SolidColorBrush(Color.FromRgb(176, 224, 230));

                listTitle.Add(new TextBlock());
                listDescription.Add(new TextBlock());
                listImg.Add(new Image());
                listButton.Add(new Button());
                listButtonNewVersion.Add(new Button());

                listTitle[i].HorizontalAlignment = HorizontalAlignment.Center;
                listTitle[i].Text = String.Format("{0} {1}", json.games[i].name, json.games[i].version);
                listTitle[i].FontSize = 20;
                listStackPanel[i].Children.Add(listTitle[i]);

                listImg[i].HorizontalAlignment = HorizontalAlignment.Center;
                listImg[i].VerticalAlignment = VerticalAlignment.Center;
                listImg[i].Source = new BitmapImage(new Uri(string.Format(@"{0}", json.games[i].thumbnail), UriKind.RelativeOrAbsolute));
                listImg[i].Width = 200;
                listImg[i].Height = 200;
                listStackPanel[i].Children.Add(listImg[i]);

                listDescription[i].HorizontalAlignment = HorizontalAlignment.Center;
                listDescription[i].Text = json.games[i].description;
                listDescription[i].Height = 80;
                listDescription[i].FontSize = 15;
                listDescription[i].TextAlignment = TextAlignment.Center;
                listDescription[i].TextWrapping = TextWrapping.Wrap;
                listStackPanel[i].Children.Add(listDescription[i]);

                listButton[i].HorizontalAlignment = HorizontalAlignment.Center;
                listButton[i].Content = "Zainstaluj";
                listButton[i].Name = "button_" + i.ToString();
                listButton[i].Click += localSaveGame;
                listButton[i].FontSize = 15;
                listButton[i].Width = 120;

                Visibility v = Visibility.Hidden;
                if (!checkDownloadGame((string)json.games[i].name))
                {
                    listButton[i].IsEnabled = false;
                    listButton[i].Content = "Zainstalowano";
                    if(json.games[i].version != localRead.getVersionForGameName((string)json.games[i].name))
                    {
                        v = Visibility.Visible;
                    }
                }
                listStackPanel[i].Children.Add(listButton[i]);

                listButtonNewVersion[i].HorizontalAlignment = HorizontalAlignment.Center;
                listButtonNewVersion[i].Content = "Pobierz nową wersje!";
                listButtonNewVersion[i].Name = "button_update_" + i.ToString();
                listButtonNewVersion[i].Click += localSaveGame;
                listButtonNewVersion[i].FontSize = 15;
                listButtonNewVersion[i].Width = 180;
                listButtonNewVersion[i].Visibility = v;
                listStackPanel[i].Children.Add(listButtonNewVersion[i]);

                this.LayoutRoot.Children.Add(listStackPanel[i]);
                x += 320;         
            }
        }

        public void getGames()
        {
            string sURL = "http://localhost:3001/games/fetchallgames";
            WebRequest wrGETURL = WebRequest.Create(sURL);

            Stream objStream = wrGETURL.GetResponse().GetResponseStream();
            StreamReader objReader = new StreamReader(objStream);
            string result = objReader.ReadToEnd();

            json = JsonConvert.DeserializeObject(result);

        }

        public void localSaveGame(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            string name = clicked.Name;
            int id = Int32.Parse(name.Substring(name.Length - 1));

            localSave.saveGameName((string)json.games[id].name);
            localSave.saveGameVersion((string)json.games[id].name, (string)json.games[id].version);
            localSave.saveGameDescription((string)json.games[id].name, (string)json.games[id].description);

            localSave.downloadGameThumbnail((string)json.games[id].name, (string)json.games[id].thumbnail);
            localSave.downloadGameExe((string)json.games[id].name, (string)json.games[id].game);

            MessageBoxResult result = MessageBox.Show("Pobieranie gry.");
            if (name.Contains("update"))
            {
                clicked.Visibility = Visibility.Hidden;
            }
            else
            {
                clicked.IsEnabled = false;
                clicked.Content = "Zainstalowano";
            }

        }

        public bool checkDownloadGame(string name)
        {
            localRead.getFilesName();
            foreach (var nameFile in localRead.namesFiles)
            {
                if(nameFile == name)
                {
                    return false;
                }
            }
            return true;
        }
    }
}
