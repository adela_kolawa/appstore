﻿using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace AppStore
{
    public partial class Window3 : Window
    {
        LocalRead localRead = new LocalRead();
        int games;
        string path = System.AppDomain.CurrentDomain.BaseDirectory;
        public Window3()
        {
            games = localRead.countGame();
            InitializeComponent();
            generateView(games);
        }

        private void generateView(int number)
        {

            List<TextBlock> listTitle = new List<TextBlock>();
            List<TextBlock> listDescription = new List<TextBlock>();
            List<Image> listImg = new List<Image>();
            List<Button> listButton = new List<Button>();
            List<StackPanel> listStackPanel = new List<StackPanel>();
            int stackPanelX = 50;
            int stackPanelY = 30;
            int y = 0;
            int x = 0;
            for (int i = 0; i < number; i++)
            {
                if (i > 1 && i % 4 == 0)
                {
                    y += 350;
                    x = 0;
                }
                listStackPanel.Add(new StackPanel());
                listStackPanel[i].VerticalAlignment = VerticalAlignment.Top;
                listStackPanel[i].HorizontalAlignment = HorizontalAlignment.Left;
                listStackPanel[i].Width = 300;
                listStackPanel[i].Height = 400;
                listStackPanel[i].Margin = new Thickness(stackPanelX + x, stackPanelY + y, 0, 0);
                listStackPanel[i].Background = new SolidColorBrush(Color.FromRgb(176, 224, 230));

                listTitle.Add(new TextBlock());
                listDescription.Add(new TextBlock());
                listImg.Add(new Image());
                listButton.Add(new Button());

                listTitle[i].HorizontalAlignment = HorizontalAlignment.Center;
                listTitle[i].Text = localRead.namesGames[i]+" "+ localRead.versionsGames[i];
                listTitle[i].FontSize = 20;
                listStackPanel[i].Children.Add(listTitle[i]);

                listImg[i].HorizontalAlignment = HorizontalAlignment.Center;
                listImg[i].VerticalAlignment = VerticalAlignment.Center;
                listImg[i].Source = new BitmapImage(new Uri(path + localRead.images[i].Source.ToString(), UriKind.RelativeOrAbsolute));
                listImg[i].Width = 200;
                listImg[i].Height = 200;
                listStackPanel[i].Children.Add(listImg[i]);

                listDescription[i].HorizontalAlignment = HorizontalAlignment.Center;
                listDescription[i].Text = localRead.descriptionGames[i];
                listDescription[i].Height = 80;
                listDescription[i].FontSize = 15;
                listDescription[i].TextAlignment = TextAlignment.Center;
                listDescription[i].TextWrapping = TextWrapping.Wrap;
                listStackPanel[i].Children.Add(listDescription[i]);

                listButton[i].HorizontalAlignment = HorizontalAlignment.Center;
                listButton[i].Content = "Graj";
                listButton[i].Name = "button_" + i.ToString();
                listButton[i].Click += runApp;
                listButton[i].FontSize = 15;
                listButton[i].Background = new SolidColorBrush(Color.FromRgb(65, 105, 225));
                listButton[i].Width = 100;
                listStackPanel[i].Children.Add(listButton[i]);

                this.LayoutRoot.Children.Add(listStackPanel[i]);
                x += 320;
            }
        }

        public void runApp(object sender, RoutedEventArgs e)
        {
            Button clicked = (Button)sender;
            string name = clicked.Name;
            int id = Int32.Parse(name.Substring(name.Length - 1));

            Process.Start(path + localRead.getPathToExe(localRead.namesFiles[id]));             
        }

        private void button1_Click(object sender, RoutedEventArgs e)
        {

            (new MainWindow()).Show();
            this.Close();
        }
    }
}
