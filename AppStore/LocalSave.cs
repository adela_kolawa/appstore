﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AppStore
{
    class LocalSave
    {

        public void downloadGameThumbnail(string name, string url)
        {
            string path = String.Format(@"Games/Image/{0}", MainWindow.login);
            try
            {
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }

            }
            catch (Exception e)
            {}
            string imageUrl = string.Format(@"{0}", url);
            string pathToSave = String.Format(@"Games/Image/{0}\{1}.jpg", MainWindow.login, name);

            FileInfo file = new FileInfo(pathToSave);

            file.Delete();

            System.Net.WebClient client = new System.Net.WebClient();
            client.DownloadFile(imageUrl, pathToSave);
        }
        public void downloadGameExe(string name, string url)
        {
            string path = String.Format(@"Games/Exe/{0}", MainWindow.login);
            try
            {
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }

            }
            catch (Exception e)
            { }
            string exeUrl = string.Format(@"{0}", url);
            string pathToSave = String.Format(@"Games/Exe/{0}\{1}.exe", MainWindow.login, name);

            System.Net.WebClient client = new System.Net.WebClient();
            client.DownloadFile(exeUrl, pathToSave);
        }

        public void saveGameName(string name)
        {
            string path = String.Format(@"Games/Name/{0}", MainWindow.login);
            try
            {
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }

            }
            catch (Exception e)
            { }
            string pathToSave = String.Format(@"Games/Name/{0}\{1}.txt", MainWindow.login, name);
            string context = name;
            FileInfo file = new FileInfo(pathToSave);

            file.Delete();
            System.IO.File.WriteAllText(pathToSave, context);
        }

        public void saveGameVersion(string name, string version)
        {
            string path = String.Format(@"Games/Version/{0}", MainWindow.login);
            try
            {
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }

            }
            catch (Exception e)
            { }
            string pathToSave = String.Format(@"Games/Version/{0}\{1}.txt", MainWindow.login, name);
            string context = version;
            System.IO.File.WriteAllText(pathToSave, context);
        }

        public void saveGameDescription(string name, string description)
        {
            string path = String.Format(@"Games/Description/{0}", MainWindow.login);
            try
            {
                if (!Directory.Exists(path))
                {
                    DirectoryInfo di = Directory.CreateDirectory(path);
                }

            }
            catch (Exception e)
            { }
            string pathToSave = String.Format(@"Games/Description/{0}\{1}.txt", MainWindow.login, name);
            System.IO.File.WriteAllText(pathToSave, description);
        }
    }
}
